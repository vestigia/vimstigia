DIST_NAME=$(lsb_release -a 2>/dev/null | awk '/Distributor ID/ {print $3}')

echo "Distro name: $DIST_NAME"
# Para debian
if [ $DIST_NAME = "Debian" ]; then
    echo "Installing on debian..."
    command -v sudo >/dev/null 2>&1 || { echo >&2 "sudo required but it's not installed.  Aborting."; exit 1; }
    sudo apt-get -y install curl
    sudo su -c "curl -sL https://deb.nodesource.com/setup | bash -"
    sudo apt-get -y install git mercurial vim-gtk vim vim-nox nodejs nodejs-legacy ruby ruby-dev
    sudo update-alternatives --install /usr/bin/node nodejs /usr/bin/nodejs 100
#    curl https://www.npmjs.org/install.sh | sudo sh
fi

# Para Ubuntu
if [ $DIST_NAME = "Ubuntu" ]; then
    echo "Installing on ubuntu..."
    command -v sudo >/dev/null 2>&1 || { echo >&2 "sudo required but it's not installed.  Aborting."; exit 1; }
    sudo apt-get -y install git nodejs mercurial vim-gtk vim vim-nox ruby ruby-dev
fi


# Para Fedora
if [ $DIST_NAME = "Fedora" ]; then
    echo "Installing on fedora..."
    command -v sudo >/dev/null 2>&1 || { echo >&2 "sudo required but it's not installed.  Aborting."; exit 1; }
    sudo yum install mercurial vim vim-X11 nodejs ruby ruby-devel
fi


# Para OpenSuse
if [ $DIST_NAME = "openSUSE" ]; then
    echo "Installing on openSUSE..."
    command -v sudo >/dev/null 2>&1 || { echo >&2 "sudo required but it's not installed.  Aborting."; exit 1; }
    sudo zypper install mercurial vim vim-X11 nodejs ruby ruby-devel
fi

command -v node >/dev/null 2>&1 || { echo >&2 "Nodejs required but it's not installed.  Aborting."; exit 1; }
command -v npm >/dev/null 2>&1 || { echo >&2 "npm required but it's not installed.  Aborting."; exit 1; }
command -v git >/dev/null 2>&1 || { echo >&2 "git required but it's not installed.  Aborting."; exit 1; }
command -v hg >/dev/null 2>&1 || { echo >&2 "mercurial required but it's not installed.  Aborting."; exit 1; }
command -v ruby >/dev/null 2>&1 || { echo >&2 "ruby required but it's not installed.  Aborting."; exit 1; }

mkdir -p ~/.vim/colors
cp colors/* ~/.vim/colors/
mkdir -p ~/.vim/snippets
cp snippets/* ~/.vim/snippets/
cp vimrc ~/.vimrc

# Install vim pathogen for plugins
mkdir -p ~/.vim/autoload ~/.vim/bundle && \
curl -LSso ~/.vim/autoload/pathogen.vim https://tpo.pe/pathogen.vim

mkdir -p ~/.vim/{autoload,bundle}
cd ~/.vim/
git init

# Vim cool addons
git clone https://github.com/tpope/vim-surround.git        ~/.vim/bundle/surround
git clone https://github.com/sjl/gundo.vim.git             ~/.vim/bundle/gundo
git clone https://github.com/fs111/pydoc.vim.git           ~/.vim/bundle/pydoc
git clone https://github.com/vim-scripts/pep8.git          ~/.vim/bundle/pep8
git clone https://github.com/vim-scripts/TaskList.vim.git  ~/.vim/bundle/tasklist
git clone https://github.com/scrooloose/nerdtree.git       ~/.vim/bundle/nerdtree
git clone https://github.com/scrooloose/nerdcommenter.git  ~/.vim/bundle/nerdcommenter

# Minibuf expl
mkdir -p ~/.vim/bundle/minibufexpl/plugin
cd ~/.vim/bundle/minibufexpl/plugin && curl http://www.vim.org/scripts/download_script.php?src_id=3640 > minibufexpl.vim

# Jedi-vim python autocompletion
sudo pip install jedi
cd ~/.vim/bundle/ && git clone --recursive https://github.com/davidhalter/jedi-vim.git

# Snipmate
git clone https://github.com/ervandew/snipmate.vim.git ~/.vim/bundle/snipmate

# File search/open FuzzyFinder
#hg clone https://bitbucket.org/ns9tks/vim-fuzzyfinder ~/.vim/bundle/fuzzyfinder
#hg clone https://bitbucket.org/ns9tks/vim-l9          ~/.vim/bundle/l9


# File search/open Command-T
# Vim debe estar compilado con ruby support. Ruby debe estar instalado en la PC
cd ~/.vim && git clone git://git.wincent.com/command-t.git bundle/command-t
cd ~/.vim/bundle/command-t/ruby/command-t && ruby extconf.rb && make

# Javascript code navigation
cd ~/.vim/bundle && git clone https://github.com/marijnh/tern_for_vim.git
cd ~/.vim/bundle/tern_for_vim && npm install
